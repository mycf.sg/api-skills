# Skills API

This repository contains code for the service `api-skills` that provides a central source-of-truth for skills used throughout the MyCareersFuture service.


- - -


## Overall Design

This repository contains 2 types of deployments:

1. Services
2. Jobs

### Services

We define 3 modes for running the Strapi service:

1. Development
2. Production Public
3. Production Admin

#### Development Mode

The development mode should only run on developers' machines. In this mode, creation/modification of content types are enabled.

#### Production Admin Mode

The production admin mode should run in production but only be made available to the development team. In this mode, creation/modification of content types are disabled, but CRUD operations on existing content types are enabled.

> Docker image can be found at [https://hub.docker.com/r/mycfsg/api-skills-admin](https://hub.docker.com/r/mycfsg/api-skills-admin). To use: `docker pull mycfsg/api-skills-admin:latest`.

#### Production Public Mode

The production public mode is purely read-only and does not contain an admin panel.

> Docker image can be found at [https://hub.docker.com/r/mycfsg/api-skills](https://hub.docker.com/r/mycfsg/api-skills). To use: `docker pull mycfsg/api-skills:latest`.

### Jobs

Jobs are available in the `./jobs` directory and are meant to be run separately from the main services. Jobs should not interact with the Strapi service without an interface (aka the API).

#### Seeder

The seeder job consumes data from a JSON file of the following format:

```json
[
  {
    "code": "%SKILL_CODE%",
    "skill": "%SKILL_LABEL%"
  },
  ...
]
```

And inserts it into the service via `POST` (create) API calls.

The seeder is available as a Docker image at [https://hub.docker.com/r/mycfsg/api-skills-job-seeder](https://hub.docker.com/r/mycfsg/api-skills-job-seeder) and to run it you'll need:

1. **`SEED_FILENAME`**: defines the path to the seed file
1. **`API_USERNAME`**: defines the username of the user used for the API
1. **`API_PASSWORD`**: defines the password of the user used for the API
1. **`API_BASE_URL`**: defines the base URL of the API without a trailing slash, locally this would be `"http://localhost:1337"`

To run this job, you need to run the following in the same network as the Strapi service (replace `strapi-service` with the hostname of your Strapi service):

```sh
docker run -it \
  -v $(pwd)/data/seed.json:/seed.json \
  mycfsg/api-skills-job-seeder \
    /seed.json api password http://strapi-service:1337
```

- - -


## Development

### Base Setup

Run `make init` to provision the environment required. This should start a MySQL server. **You'll need this for all future steps**.

To access the database via command line on your host, run `make access_db`.

### Production Replication

Run `make start` to start two copies of the same Strapi application, one with the administration panel, and one without. These replicate production locally.

| Service | On Host | On Guest | Access |
| --- | --- | --- | -- |
| Strapi Admin | `localhost:13370` | `application_admin:13370` | [`localhost:13370/admin`](http://localhost:13370/admin) |
| Strapi Public | `localhost:13371` | `application_public:13371` | [`localhost:13371`](http://localhost:13371) | 

### Local Development

Run `make dev` to start the application in development mode which will allow you to add/modify content types. The service will be made available at [`localhost:1337`](http://localhost:1337).


- - -


## Deployment Notes

For the following instructions, substitue `localhost:13370` with your domain name for the admin interface, and `localhost:13371` with the domain name for the public API.

### Setting up volumes

The following volumes should be shared between Production Admin and Production Public:

1. **`./extensions/users-permissions/config/jwt.json`**: this enables an API key generated from the admin mode to be used on the public-facing service

### Setting up environment

The following environment variables should be set when running in production:

- **`DATABASE_HOST`**: defines the hostname of the database
- **`DATABASE_PORT`**: defines the port the database server is listening on
- **`DATABASE_NAME`**: defines the name of the database schema to use
- **`DATABASE_USERNAME`**: defines the username of the database user
- **`DATABASE_PASSWORD`**: defines the password for the database user

### Setting up public access

1. Navigate to [http://localhost:13370/admin](http://localhost:13370/admin)
2. Create an administration account and save your credentials in your team's shared space
3. Navigate to [http://localhost:13370/admin/plugins/users-permissions/roles](http://localhost:13370/admin/plugins/users-permissions/roles) and click on **Public**
4. Under the *Permissions* header, click on the *APPLICATION* acordion to reveal the *Skill* content type
5. Check **count**, **find**, **findone**

### Setting up automation

1. Navigate to [http://localhost:13370/admin](http://localhost:13370/admin)
2. Create an administration account and save your credentials in your team's shared space
3. Navigate to [http://localhost:13370/admin/plugins/users-permissions/roles](http://localhost:13370/admin/plugins/users-permissions/roles) and click on **Authenticated**
4. Under the *Permissions* header, click on the *APPLICATION* acordion to reveal the *Skill* content type
5. Check **count**, **find**, **create**, **findone**, **delete**, **update**
6. On the left navigation, click on **Users** which should bring you to [http://localhost:13370/admin/plugins/content-manager/plugins::users-permissions.user](http://localhost:13370/admin/plugins/content-manager/plugins::users-permissions.user)
7. Click on the **+ Add New User** button on the top right of the page
8. Set `api` for the **Username**
9. Set `api-skills@yourdomain.com` for the **Email**
10. Set `password` for the **Password** (PSA: change it to something reasonably more secure for production)
11. Hit the **Save** button
12. Run the following `curl` command to retrieve the JWT you need (substitute the password for your own):
  ```sh
  curl -X POST -H 'Content-Type: application/json' -d '{"identifier":"api","password":"password"}' 'http://localhost:13370/auth/local' | jq '.jwt' -r
  ```
13. To access an endpoint that requires authentication, use the JWT returned in the response from the above command in the call to the public API (replace the example JWT with your own):
  ```sh
  curl -X POST -H 'Content-Type: application/json' -H 'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwiaWF0IjoxNTgyMDg0OTc1LCJleHAiOjE1ODQ2NzY5NzV9.DkoTzLpSKu8KHADdx1oQ78xcW6I4AdlyGNE0qPgjU2o' -d '{"code":"__test","skill":"__skill"}' 'http://localhost:13371/skills'
  ```
14. Or else in one command:
  ```sh
  curl -X POST -H 'Content-Type: application/json' -H "Authorization: Bearer $(curl -X POST -H 'Content-Type: application/json' -d '{\"identifier\":\"api\",\"password\":\"password\"}' 'http://localhost:13370/auth/local' | jq '.jwt' -r)" -d '{"code":"__test","skill":"__skill"}' 'http://localhost:13371/skills'
  ```

## License
