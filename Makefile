IMAGE_REGISTRY=docker.io
IMAGE_NAMESPACE=mycfsg
IMAGE_NAME=api-skills
IMAGE_TAG=$$(git rev-parse --verify HEAD | head -c 12)-$$(date +'%Y%m%d%H%m%S')

SEEDER_USERNAME=username
SEEDER_PASSWORD=password
SEEDER_SOURCE_FILE=$$(pwd)/data/20191121.json
SEEDER_TARGET_URL=http://localhost:1337

# -------------------------------------------------------------------
# use the following file to override anything defined above this line
-include Makefile.properties

init:
	USER_ID=$$(id -u) \
		docker-compose \
			-f ./deploy/base.yml \
			up
access_db:
	docker exec -it \
		$$(docker ps | grep mysql | grep 53306 | cut -f 1 -d ' ') \
		mysql -uroot -ptoor -h0.0.0.0 -P3306 api-skills
denit:
	docker-compose -f ./deploy/base.yml down
dev:
	cd ./src && yarn develop
seed: build_job_seeder
	# WARNING: this only runs with certainty on linux
	docker run -it \
		--network host \
		-v $(SEEDER_SOURCE_FILE):/seed.json \
		-v /etc/resolv.conf:/etc/resolv.conf \
		$(IMAGE_NAMESPACE)/$(IMAGE_NAME):job-seed \
			/seed.json \
			"$(SEEDER_USERNAME)" \
			"$(SEEDER_PASSWORD)" \
			"$(SEEDER_TARGET_URL)"

start:
	USER_ID=$$(id -u) \
		docker-compose \
			-f ./deploy/application.yml \
			up \
			--build
stop:
	docker-compose -f ./deploy/application.yml down
clean:
	rm -rf \
		./deploy/data/mysql/* \
		./src/build \
		./src/node_modules

build_public:
	docker build \
		--tag $(IMAGE_NAMESPACE)/$(IMAGE_NAME):public \
		--target public \
		./src

publish_public: build_public
	docker tag $(IMAGE_NAMESPACE)/$(IMAGE_NAME):public \
		$(IMAGE_REGISTRY)/$(IMAGE_NAMESPACE)/$(IMAGE_NAME):latest
	docker push $(IMAGE_REGISTRY)/$(IMAGE_NAMESPACE)/$(IMAGE_NAME):latest
	docker tag $(IMAGE_NAMESPACE)/$(IMAGE_NAME):public \
		$(IMAGE_REGISTRY)/$(IMAGE_NAMESPACE)/$(IMAGE_NAME):$(IMAGE_TAG)
	docker push $(IMAGE_REGISTRY)/$(IMAGE_NAMESPACE)/$(IMAGE_NAME):$(IMAGE_TAG)

build_admin:
	docker build \
		--tag $(IMAGE_NAMESPACE)/$(IMAGE_NAME):admin \
		--target admin \
		./src

publish_admin: build_admin
	docker tag $(IMAGE_NAMESPACE)/$(IMAGE_NAME):admin \
		$(IMAGE_REGISTRY)/$(IMAGE_NAMESPACE)/$(IMAGE_NAME)-admin:latest
	docker push $(IMAGE_REGISTRY)/$(IMAGE_NAMESPACE)/$(IMAGE_NAME)-admin:latest
	docker tag $(IMAGE_NAMESPACE)/$(IMAGE_NAME):admin \
		$(IMAGE_REGISTRY)/$(IMAGE_NAMESPACE)/$(IMAGE_NAME)-admin:$(IMAGE_TAG)
	docker push $(IMAGE_REGISTRY)/$(IMAGE_NAMESPACE)/$(IMAGE_NAME)-admin:$(IMAGE_TAG)

build_job_seeder:
	docker build \
		--tag $(IMAGE_NAMESPACE)/$(IMAGE_NAME):job-seed \
		./jobs/seed

publish_job_seeder: build_job_seeder
	docker tag $(IMAGE_NAMESPACE)/$(IMAGE_NAME):job-seed \
		$(IMAGE_REGISTRY)/$(IMAGE_NAMESPACE)/$(IMAGE_NAME)-job-seeder:latest
	docker push $(IMAGE_REGISTRY)/$(IMAGE_NAMESPACE)/$(IMAGE_NAME)-job-seeder:latest
	docker tag $(IMAGE_NAMESPACE)/$(IMAGE_NAME):job-seed \
		$(IMAGE_REGISTRY)/$(IMAGE_NAMESPACE)/$(IMAGE_NAME)-job-seeder:$(IMAGE_TAG)
	docker push $(IMAGE_REGISTRY)/$(IMAGE_NAMESPACE)/$(IMAGE_NAME)-job-seeder:$(IMAGE_TAG)
