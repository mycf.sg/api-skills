#!/bin/sh
FILEPATH="$1";
if [ "${FILEPATH}" = "" ]; then
  printf -- 'missing FILEPATH (first argument)\n';
  exit 1;
fi
USERNAME="$2";
if [ "${USERNAME}" = "" ]; then
  printf -- 'missing USERNAME (second argument)\n';
  exit 2;
fi
PASSWORD="$3";
if [ "${PASSWORD}" = "" ]; then
  printf -- 'missing PASSWORD (third argument)\n';
  exit 3;
fi
BASE_URL="$4";
if [ "${BASE_URL}" = "" ]; then
  printf -- 'missing BASE_URL (fourth argument)\n';
  exit 4;
fi;

PING="$(curl ${BASE_URL})";
if [ "$?" != "0" ] ; then
  printf -- "BASE_URL '${BASE_URL}' does not seem to be alive\n";
  exit 5;
fi;

AUTH_RESPONSE="$(curl -X POST \
  -H 'Content-Type: application/json' \
  -d "{\"identifier\":\"${USERNAME}\",\"password\":\"${PASSWORD}\"}" \
  "${BASE_URL}/auth/local")";
if [ "$?" != "0" ]; then
  printf -- "authentication request failed for user '${USER}' and provided password\n";
  exit 6;
fi;
printf -- "AUTH_RESPONSE=${AUTH_RESPONSE}\n";

JWT_TOKEN=`printf -- "${AUTH_RESPONSE}" | jq '.jwt' -r`;
if [ "${JWT_TOKEN}" = "" ]; then
  printf -- "logical authentication failed for user '${USER}' and provided password\n";
  exit 7;
fi;
printf -- "JWT_TOKEN=${JWT_TOKEN}\n";

if [ -f $1 ]; then
  SEED=$(cat $1);
  VALUES="$(echo "$SEED" | jq '.[] | [.code, .skill] | @csv' -r | tr -s ',' '|' | tr -d '"')"
  printf -- "$VALUES\n" | while read -r value; do
    CODE=$(printf -- "$value" | cut -f 1 -d '|');
    SKILL=$(printf -- "$value" | cut -f 2 -d '|');
    printf -- "seeding [CODE:${CODE}, SKILL:${SKILL}]...\n";
    RESPONSE="$(curl -s -X POST \
      -H 'Content-Type: application/json; charset=utf8' \
      -H "Authorization: Bearer ${JWT_TOKEN}" \
      -d "{\"code\":\"${CODE}\",\"skill\":\"${SKILL}\"}" \
      "${BASE_URL}/skills")";
    if [ "$?" != "0" ]; then
      printf -- "\033[31m> ERRORED while seeding [CODE:${CODE}, SKILL:${SKILL}]\033[0m\n";
    elif [ "$(printf -- "${RESPONSE}" | jq '.error' -r)" != "null" ]; then
      printf -- "\033[31m> FAILED to seed [CODE:${CODE}, SKILL:${SKILL}]\033[0m\n";
      ERROR_TYPE=$(printf -- "${RESPONSE}" | jq '.error' -r);
      ERROR_MESSAGE=$(printf -- "${RESPONSE}" | jq '.message' -r);
      printf -- "\033[31m  - ${ERROR_TYPE}: \033[1m${ERROR_MESSAGE}\033[0m\n";
    else
      printf -- "\033[32m> seeded [CODE:${CODE}, SKILL:${SKILL}]\033[0m\n";
    fi;
  done;
else
  printf -- "the file '$1' could not be found\n";
  exit 127;
fi;
