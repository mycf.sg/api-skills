FROM node:10-alpine AS public
ARG USER_ID=root
RUN apk update --no-cache \
  && apk upgrade --no-cache
RUN mkdir /app
RUN apk add --no-cache shadow \
  && if ! cat /etc/passwd | grep ${USER_ID}; then \
      adduser -u ${USER_ID} -D -h /app ${USER_ID}; fi \
  && chown ${USER_ID}:${USER_ID} -R /app \
  && apk del --no-cache shadow
RUN chown ${USER_ID} -R /app
USER ${USER_ID}
COPY --chown=${USER_ID}:root ./package.json /app
COPY --chown=${USER_ID}:root ./yarn.lock /app
RUN cd /app && yarn --production
COPY --chown=${USER_ID}:root ./api /app/api
COPY --chown=${USER_ID}:root ./config /app/config
COPY --chown=${USER_ID}:root ./extensions /app/extensions
COPY --chown=${USER_ID}:root ./public /app/public
WORKDIR /app
ENTRYPOINT [ "yarn", "start" ]
EXPOSE 1337

FROM public AS admin
ARG USER_ID=root
USER ${USER_ID}
RUN cd /app \
  && yarn \
  && yarn build
